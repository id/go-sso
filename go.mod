module git.autistici.org/id/go-sso

go 1.14

require (
	github.com/gorilla/securecookie v1.1.2
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
)
